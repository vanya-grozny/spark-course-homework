package course.spark.homework2

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types._

/**
  * Analyse events, produced by WikipediaEditLoader job. In contrast to the latter,
  * Spark Structured Streaming API is used here, which is incompatible with DStreams API
  * (see https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html)
  */
object WikipediaEditAnalyzer {
  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      throw new Exception("Base dir for checkpoint, input and output must be specified")
    }
    val baseDir: String = args(0)

    implicit val spark: SparkSession = SparkSession.builder
      .master("local[2]")
      .appName("WikipediaEditAnalyzer")
      .getOrCreate()

    // In contrast to Spark batch processing streams require manually specified schema
    // for input Dataset
    val schema = StructType(
      StructField("timestamp", LongType) ::
      StructField("channel", StringType) ::
      StructField("user", StringType) ::
      StructField("title", StringType) ::
      Nil
    )

    // Read `|` separated files, written by WikipediaEditLoader to <baseDir>/input directory.
    // Make sure to use the same baseDir, as WikipediaEditLoader uses and have WikipediaEditLoader
    // running simultaneously with WikipediaEditAnalyzer (WikipediaEditAnalyzer won't read old files,
    // only those changed during its execution)
    val inputData = spark.readStream
      .option("header", "false")
      .option("sep", "|")
      .schema(schema)
      .csv(baseDir + "/input/file-*/part-*")

    // TODO: Task 2: write transformation to print out top 100 categories every minute by number of changes
    // Hint: Use fields added in WikipediaEditLoader earlier
    val perWindowTitleLengths = inputData
      .transform(perWindowTitleLength())

    // Write results to output dir. Output dir can be local file while running in local mode
    // or HDFS (S3, Google Cloud Storage and so on) for production environment.
    val query = perWindowTitleLengths
      .repartition(1)
      .writeStream
      .format("csv")
       .option("checkpointLocation", baseDir + "/checkpoint")
      .option("path", baseDir + "/output")
      .outputMode(OutputMode.Append())
      .start()

    query.awaitTermination()
  }

  // This example shows how to compute number of edits grouped by 10 seconds tumbling window
  // and page title lengths. `window` column will be a struct containing start and end fields
  // after groupBy.
  def perWindowTitleLength()(df: DataFrame)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._

    df
      .withColumn("unix_timestamp", ($"timestamp" / 1000).cast(TimestampType))
      // If current_time - event_time > 10 minutes, Spark has a right to ignore this event
      .withWatermark("unix_timestamp", "60 seconds")
      .groupBy(
        window(
          $"unix_timestamp",
          "10 seconds",
          "10 seconds"
        ).as("window"),
        length($"title").as("title_length")
      )
      .agg(count("*").as("count"))
      .select(
        date_format($"window.start", "YYYY-MM-dd'T'HH:mm:ss").as("start"),
        date_format($"window.end", "YYYY-MM-dd'T'HH:mm:ss").as("end"),
        $"title_length",
        $"count"
      )
  }
}
