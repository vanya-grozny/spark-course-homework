package course.spark.homework2

import course.spark.homework2.inputsource.{WikipediaEditEvent, WikipediaEditReceiver}
import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.ReceiverInputDStream

object WikipediaEditLoader {
  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      throw new Exception("Base dir for checkpoint, input and output must be specified")
    }
    val baseDir: String = args(0)

    val conf = new SparkConf()
      .setMaster("local[2]")
      .setAppName("WikipediaEditLoader")

    // Using 10 seconds batch duration for micro-batches
    val ssc = new StreamingContext(conf, Seconds(10))

    // Receive Stream of page edits from Wikipedia's IRC channel irc.wikimedia.org
    // We get a DStream of WikipediaEditEvent objects. Read more about DStreams here:
    // https://spark.apache.org/docs/latest/streaming-programming-guide.html
    val wikipediaEditStream: ReceiverInputDStream[WikipediaEditEvent] = ssc
      .receiverStream(new WikipediaEditReceiver())

    // Output Wikipedia edits to `<baseDir>/input` dir. For each micro-batch Spark will create
    // separate directory (with prefix `file-` as we specified), containing csv files for each
    // partition. All the files created by Spark can be listed using bash command:
    // ```
    // ls <baseDir>/input/file-*/part-*
    // ```
    // TODO: Task 1: use any http client you like to enrich each record with list of parent categories
    // Hint: edit.getDiffUrl has a form of
    // ```
    // https://en.wikipedia.org/w/index.php?diff=865996154&oldid=839882534
    // ```
    // You can use oldid from the url above to query page categories:
    // ```
    // https://en.wikipedia.org/w/api.php?action=query&revids=865996139&prop=categories
    // ```
    // And then parent categories for each category:
    // ```
    // https://en.wikipedia.org/w/api.php?action=query&titles=&prop=categories
    // ```
    wikipediaEditStream
      .map { edit =>
        Seq(
          edit.getTimestamp.toString,
          edit.getChannel,
          edit.getUser,
          edit.getTitle,
          edit.getDiffUrl
        ).mkString("|")
      }
      .repartition(2)
      .saveAsTextFiles(baseDir + "/input/file-")

    ssc.start()
    ssc.awaitTermination()
  }
}
